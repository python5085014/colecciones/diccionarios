# dict(clave,valor)

diccionario = {
    "IDE" : "Integrated Development Environment",
    "OPP" : "Object Oriented Programming",
    "DBMS" : "Database Manadement System"
}

# mostramos diccionario
print(diccionario)

# longitud del diccionario 
print(len(diccionario))

# acceder a un elemento(key)
print(diccionario["IDE"])
print(diccionario.get("OPP"))

# agregar un nuevo elemento
diccionario["PK"] = "Primary Key"

# modificar elementos
diccionario["OPP"] = "Programación Orientada Objetos"
print(diccionario)

# remover un elemento
diccionario.pop("PK")
print(diccionario)

# recorrer los elementos
for clave, valor in diccionario.items():
    print(clave,":",valor)

# mostrar solo los elementos clave
for clave in diccionario.keys():
    print(clave)   

# mostrar solo los elementos valor
for valor in diccionario.values():
    print(valor)  

# limpiar el diccionario
diccionario.clear()  
print(diccionario)        