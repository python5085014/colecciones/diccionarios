# Dicionarios en Python


### Los diccionarios en Python, al igual que las listas y las tuplas, nos permiten almacenar diferentes tipos de datos: `Strings`, `enteros`, `flotantes`, `booleanos` , `tuplas`, `listas` e inclusive otros `diccionarios`.

### Los diccionario son mutables, es decir, es posible modificar su longitud, podemos agregar o quitar elementos de él; de igual forma todos los valores almacenados en el diccionario pueden ser modificados.

<br>
<br>

![Descripción de la imagen](dict.PNG)

